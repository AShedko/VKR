class SeqModel(nn.Module):
    def __init__(self, num_ftrs, hidden, transf, num_layers=N_layers):
        super(SeqModel, self).__init__()
        self.hidden = hidden
        self.num_ftrs = num_ftrs
        # Store parameters and other stuff
        self.emb = transf
        # setting bidirectional to true makes the net bidirectional (duh)
        self.L = nn.LSTM(num_ftrs, hidden,
                         num_layers=num_layers,
			 bidirectional = True).to(device)
        self.h2la = nn.Linear(2*hidden, len(class_names)).to(device)
        self.drop = nn.Dropout(0.3)
        self.h = (torch.zeros(2*N_layers, BATCH ,hidden).to(device),
                  torch.zeros(2*N_layers,BATCH ,hidden).to(device))

    def init_hidden(self):
        self.h = (torch.randn(self.num_layers,
                              BATCH ,self.hidden).to(device),
                  torch.randn(self.num_layers,
                              BATCH ,self.hidden).to(device))

    # define forward pass (backwards is automatic)
    def forward(self,inp):
        # transform all input imgs with embedder
        X = torch.stack(
            [self.emb(inp[i].to(device)) for i in range(len(inp))]
        ).squeeze()
        # Apply LSTM
        o,h = self.L(X.view(len(inp), -1 , self.num_ftrs),self.h)
        # Apply hidden -> result transform and average the predictions
        res = nn.Softmax(dim=1)(self.h2la(self.drop(o)).mean(0))
        return res
