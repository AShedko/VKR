class Ensemble(nn.Module):
    def __init__(self, ms, out_dim):
        super(Ensemble, self).__init__()
        self.ms = ms
        self.out_dim = out_dim
	# wieghts and bias for each model in ensamble
        self.lin  = [
		nn.Linear(out_dim,out_dim).to(device) 
		for i in range(len(ms))
	]
        for l in self.lin:
	#  initially the model is just an average
            l.weight = torch.nn.Parameter(
			torch.ones(out_dim,out_dim, device=device))
            l.bias = torch.nn.Parameter(
			torch.zeros(out_dim, device=device))
        self.k  = len(ms)
        
    def forward(self, Xs):
        # initial result    	 batch , out 
        res = torch.zeros(Xs[0].size(0), self.out_dim, 
			  requires_grad=True).to(device)
        for i in range(len(Xs)): # reduction
            res +=  self.lin[i%self.k](self.ms[i%self.k](Xs[i].to(device)))
        res = nn.Softmax(dim=1)(res)
        return res
