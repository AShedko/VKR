"""Dev-Server for model"""
from bottle import route, run, request
from predict import *
import PIL
import urllib

# use decorator to setup route
@route('/req', method= "GET")
def index():
    # Params are passed using url params
	res = urllib.request.urlretrieve(request.params["url"], "test.jpg")
    # read downloaded image
	im = PIL.Image.open("test.jpg")
	return {'result': str(predict(im))} # returned as json

run(host='localhost', port=5533)
