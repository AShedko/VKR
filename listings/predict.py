import torch
import torchvision.transforms as transforms
import sys

# load model and transfer it to cpu()
# then set it to eval mode (dropout = id)
model = torch.load("./models/resnet50_trained").cpu().eval()

# transform image to fit into NN
tf = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(
            [0.485, 0.456, 0.406],
            [0.229, 0.224, 0.225])
    ])

# Apply NN
def predict(img):
    img = tf(img).unsqueeze(0) # Due to the 0-th dim being the batch dim
    return model(img).max(1)[1].numpy()[0]
        # argmax then [number from tensor]
